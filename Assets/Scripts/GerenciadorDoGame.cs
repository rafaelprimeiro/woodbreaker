﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GerenciadorDoGame : MonoBehaviour {

    public static int blocos;
    public static int blocosDestruidos;
    public Image estrelas;
    public GameObject canvas;

    public Bola bola;
    public Plataforma plataforma;

    public static GerenciadorDoGame instancia;

    void Awake()
    {
        instancia = this;
    }

    void Start()
    {
        if (Application.loadedLevel == 1)
        {
            canvas.SetActive(false);
            blocosDestruidos = 0;
        }
    }

    public void finalizarJogo()
    {
        //Application.LoadLevel("CenaPrincipal");

        canvas.SetActive(true);
        estrelas.fillAmount = (float)blocosDestruidos / (float)blocos;

        plataforma.enabled = false;
        Destroy(bola.gameObject);
    }

    public void JogarNovamente()
    {
        Application.LoadLevel("CenaPrincipal");
    }

    public void AlterarCena(string cena = "CenaPrincipal")
    {
        Application.LoadLevel(cena);
    }

    public void FecharApp()
    {
        Application.Quit();
    }
}
