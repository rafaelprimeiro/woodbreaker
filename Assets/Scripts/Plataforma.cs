﻿using UnityEngine;
using System.Collections;

public class Plataforma : MonoBehaviour {

    public float velocidade;
    public float limiteX;

	// Use this for initialization
	void Start () {
        limiteX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x - GetComponent<SpriteRenderer>().bounds.extents.x;
	}
	
	// Update is called once per frame
	void Update () {

        float direcaoMouse = Input.GetAxis("Mouse X"); // -1 esquerda; +1 direita; 0 = parado
        GetComponent<Transform>().position += Vector3.right * direcaoMouse * velocidade * Time.deltaTime;

        float xAtual = transform.position.x;
        xAtual = Mathf.Clamp(xAtual, -limiteX, limiteX);
        transform.position = new Vector3(xAtual, transform.position.y, transform.position.z);
	}
}
