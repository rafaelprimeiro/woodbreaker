﻿using UnityEngine;
using System.Collections;

public class Bola : MonoBehaviour {

    public Vector3 Direcao;
    public float velocidade;
    public GameObject particulaBlocos;
    public ParticleSystem particulaFolhas;

	// Use this for initialization
	void Start () {
        Direcao.Normalize();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Direcao * velocidade * Time.deltaTime;
	}

    void OnCollisionEnter2D(Collision2D colisor)
    {
        Vector2 normal = colisor.contacts[0].normal;

        Plataforma plataforma = colisor.transform.GetComponent<Plataforma>();
        GeradorDeArestas gerador = colisor.transform.GetComponent<GeradorDeArestas>();

        if (plataforma != null)
        {
            if (normal != Vector2.up)
            {
                //Game over
                GerenciadorDoGame.instancia.finalizarJogo();
                return;
            }
            else
            {
                particulaFolhas.transform.position = colisor.transform.position;
                particulaFolhas.Play();

                if (GerenciadorDoGame.blocos == GerenciadorDoGame.blocosDestruidos)
                {
                    GerenciadorDoGame.instancia.finalizarJogo();
                }
            }
        }
        else if (gerador != null)
        {
            if (normal == Vector2.up)
            {
                //game over
                GerenciadorDoGame.instancia.finalizarJogo();
                return;
            }
        }
        else // Bloco da arvore
        {
            Bounds limiteColisor = colisor.transform.GetComponent<SpriteRenderer>().bounds;
            Vector3 posicaoEfeito = new Vector3(colisor.transform.position.x + limiteColisor.extents.x, colisor.transform.position.y - limiteColisor.extents.y, colisor.transform.position.z);
            GameObject particulas = (GameObject) Instantiate(particulaBlocos, posicaoEfeito, Quaternion.identity);
            ParticleSystem componenteParticulas = particulas.GetComponent<ParticleSystem>();
            Destroy(particulas, componenteParticulas.duration + componenteParticulas.startLifetime);
            Destroy(colisor.gameObject, 0);// depois desta linha não será mais possivel utilizar este objeto, cuidado!

            GerenciadorDoGame.blocosDestruidos++;
        }

        Direcao = Vector2.Reflect(Direcao, normal);
        Direcao.Normalize();
    }
}
