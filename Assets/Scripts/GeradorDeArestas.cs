﻿using UnityEngine;
using System.Collections;

public class GeradorDeArestas : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Camera cam = Camera.main;
        EdgeCollider2D colisor = GetComponent<EdgeCollider2D>();
        Vector2 esquerdoInfo = cam.ScreenToWorldPoint(new Vector3(0, 0, 0));
        Vector2 esquerdoSuper = cam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        Vector2 direitoSuper = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        Vector2 direitoInfo = cam.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        colisor.points = new Vector2[5] {
            esquerdoInfo,
            esquerdoSuper,
            direitoSuper,
            direitoInfo,
            esquerdoInfo
        };

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
