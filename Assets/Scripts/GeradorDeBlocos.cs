﻿using UnityEngine;
using System.Collections;

public class GeradorDeBlocos : MonoBehaviour {

    public GameObject[] blocos;
    public int linhas;

	// Use this for initialization
	void Start () {
        CriarGrupoDeBlocos();
	}

    void CriarGrupoDeBlocos()
    {
        Bounds limiteBloco = blocos[0].GetComponent<SpriteRenderer>().bounds;
        float larguraBloco = limiteBloco.size.x;
        float alturaBloco = limiteBloco.size.y;
        float larguraDaTela, alturaDaTela, multiplicadorLargura;
        int colunas;
        ColetaInformacoesDoBloco(larguraBloco, out larguraDaTela, out alturaDaTela, out colunas, out multiplicadorLargura);

        for (int i = 0; i < linhas; i++)
        {
            for (int j = 0; j < colunas; j++)
            {
                GameObject bloco = blocos[Random.Range(0, blocos.Length)];
                GameObject blocoWorld = (GameObject) Instantiate(bloco);
                blocoWorld.transform.position = new Vector3(-(larguraDaTela * 0.5f) + (j * larguraBloco * multiplicadorLargura), (alturaDaTela * 0.5f) - (i * alturaBloco), 0);
                float novaLarguraDoBloco = blocoWorld.transform.localScale.x * multiplicadorLargura;
                blocoWorld.transform.localScale = new Vector3(novaLarguraDoBloco, blocoWorld.transform.localScale.y, 1);
            }
        }

        GerenciadorDoGame.blocos = linhas * colunas;
    }

    void ColetaInformacoesDoBloco (float larguraBloco, out float larguraTela, out float alturaTela, out int colunas, out float multiplicadorLargura) 
    {
        Camera c = Camera.main;
        larguraTela = (c.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)) - c.ScreenToWorldPoint(new Vector3(0, 0, 0))).x;
        alturaTela = (c.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)) - c.ScreenToWorldPoint(new Vector3(0, 0, 0))).y;
        colunas = (int) (larguraTela/larguraBloco);

        multiplicadorLargura = larguraTela / (larguraBloco * colunas);
    }
}
